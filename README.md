# Demo instance for the reelworx/t3-mailservice library

This is a TYPO3 version 11 instance for demonstrating the usage of the reelworx/t3-mailservice library.

https://packagist.org/packages/reelworx/t3-mailservice

It features test-mails via a backend module as well as the modification
of the ext:felogin password recovery mails.

## Setup

The setup is DDEV based. So simply run `ddev start` and `ddev composer install` and you should be ready to install TYPO3.

The `packages/demo/` folder holds the local site setup package, which provides a backend module to send test mails
as well as an MJML adapter. Peek into the code to learn more.

## License

See GPL.txt shipped within this package.

(c) by Reelworx GmbH, since 2022
