<?php

declare(strict_types=1);

/**
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * @copyright Reelworx GmbH
 */

namespace Reelworx\Demo\Controller;

use Reelworx\TYPO3\FakeFrontend\FrontendUtility;
use Reelworx\TYPO3\MailService\ExtbaseMailTrait;
use Reelworx\TYPO3\MailService\MailMessage;
use Reelworx\TYPO3\MailService\MailService;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Fluid\View\StandaloneView;

class BackendController extends ActionController
{
    use ExtbaseMailTrait;

    /**
     * Backend Template Container
     *
     * @var string
     */
    protected $defaultViewObjectName = \TYPO3\CMS\Backend\View\BackendTemplateView::class;

    public function indexAction()
    {
    }

    /**
     * @throws StopActionException
     */
    public function sendMailAction(string $mailName)
    {
        // dirty thing to skip the MJML post-processing inside MjmlConnector when we don't want it
        $GLOBALS['skipMJML'] = $mailName !== 'mjml';

        // some settings that usually come from TypoScript
        $this->settings['mail'] = [
            'sender_name' => 'Mailer',
            'sender_email' => 'no-reply@example.com',
            'view' => [
                'templateRootPaths' => [ 10 => 'EXT:demo/Resources/Private/Mail/Templates/'],
                'layoutRootPaths' => [ 10 => 'EXT:demo/Resources/Private/Mail/Layouts/' ],
                'partialRootPaths' => [ 10 => 'EXT:demo/Resources/Private/Mail/Partials/' ],
            ],
        ];

        $backupRequest = $GLOBALS['TYPO3_REQUEST'] ?? null;
        FrontendUtility::buildFakeFE(1);

        /** @var MailService $mailService */
        /** @var MailMessage $msg */
        /** @var StandaloneView $mailView */
        $this->createMailMessage($mailService, $mailView, $msg);

        $mailView->assign('name', 'Frank Fortune');

        $msg->setContent($mailService->renderMail($mailView, $mailName, 'en'));

        $msg->setTo('foo@hu.there');

        $msg->send();

        if ($backupRequest) {
            $GLOBALS['TYPO3_REQUEST'] = $backupRequest;
        }

        $this->redirect('index');
    }
}
