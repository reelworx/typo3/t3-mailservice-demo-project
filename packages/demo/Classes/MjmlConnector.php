<?php

declare(strict_types=1);

/**
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * @copyright Reelworx GmbH
 */

namespace Reelworx\Demo;

use Reelworx\TYPO3\MailService\MailContent;
use Reelworx\TYPO3\MailService\MailRenderedEvent;
use Symfony\Component\Mime\Email;
use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\FrontendLogin\Event\SendRecoveryEmailEvent;

class MjmlConnector
{
    /** @var string base URL of the MJML service defined in DDEV */
    private const MJML_BASE_URL = 'http://mjml:15500/';

    /**
     * Integration of the MJML API: https://mjml.io/api
     *
     * @param MailRenderedEvent $event
     * @return void
     */
    public function __invoke(MailRenderedEvent $event): void
    {
        // dirty hack to make the processing skipable from inside our BackendController
        if ($GLOBALS['skipMJML'] ?? false) {
            return;
        }

        $html = $event->getMailContent()->html;
        $apiData = [
            'json' => ['mjml' => $html]
        ];
        $response = GeneralUtility::makeInstance(RequestFactory::class)
            ->request(self::MJML_BASE_URL . 'v1/render', 'POST', $apiData);
        if ($response->getStatusCode() === 200) {
            $mjmlResponse = json_decode($response->getBody()->getContents(), true);
            if ($mjmlResponse['errors'] === []) {
                $event->getMailContent()->html = $mjmlResponse['html'];
            } else {
                // todo log errors
            }
        }
    }

    public function felogin(SendRecoveryEmailEvent $event)
    {
        $this->processContent($event->getEmail(), $event->getEmail()->getHtmlBody(true));
    }

    protected function processContent(Email $message, $html): void
    {
        // logo injection
        $logoPath = 'EXT:demo/Resources/Public/Images/reelworx_logo.png';
        $id = md5($logoPath);
        $message->embedFromPath(GeneralUtility::getFileAbsFileName($logoPath), $id);
        $cid = 'cid:' . $id;
        $html = str_replace($logoPath, $cid, $html);

        $mailContent = new MailContent('', '', $html);
        $event = new MailRenderedEvent($mailContent);
        $this->__invoke($event);
        $message->html($mailContent->html);
    }
}
