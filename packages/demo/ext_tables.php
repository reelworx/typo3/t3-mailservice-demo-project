<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
    'Demo',
    'web', // Make module a submodule of 'web'
    'mailButtons', // Submodule key
    '', // Position
    [
        \Reelworx\Demo\Controller\BackendController::class => 'index, sendMail',
    ],
    [
        'access' => 'user,group',
        'labels' => 'LLL:EXT:demo/Resources/Private/Language/locallang_mod.xlf',
        'navigationComponentId' => '',
        'inheritNavigationComponentFromMainModule' => false,
    ]
);
