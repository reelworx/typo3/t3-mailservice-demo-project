<?php
defined('TYPO3_MODE') || die('Access denied.');


$configManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ConfigurationManager::class);

$originalConfig = $sysConfig = $configManager->getLocalConfiguration();

$sysConfig['SYS']['exceptionalErrors'] = 12290;
$sysConfig['SYS']['displayErrors'] = 1;
$sysConfig['MAIL']['defaultMailFromAddress'] = 'support@reelworx.at';
$sysConfig['MAIL']['defaultMailFromName'] = 'Demo Mailer';

if ($sysConfig !== $originalConfig) {
    $configManager->writeLocalConfiguration($sysConfig);
}

$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class);

// style BE login
$logo = 'EXT:demo/Resources/Public/Images/reelworx_logo.png';
$backendConfig = $extensionConfiguration->get('backend');
if ($backendConfig['loginLogo'] !== $logo) {
    $extensionConfiguration->set('backend', 'loginLogo', $logo);
}

$schedulerConfig = $extensionConfiguration->get('scheduler');
if ($schedulerConfig['showSampleTasks']) {
    $extensionConfiguration->set('scheduler', 'showSampleTasks', 0);
}
